================================================
mk-slab-local-salt-model Reclass Model
================================================

control/management plane hypervisor nodes
=========================================

kvm01
kvm02
kvm02

maas node
=========

mas01

config node
===========

cfg01

openstack control cluster
=========================

ctl01
ctl02
ctl03

openstack telemetry and mongodb cluster
=======================================

mdb01
mdb02
mdb03

mysql cluster
=============

dbs01
dbs02
dbs03

messaging cluster
=================

rmq01
rmq02
rmq03

proxy nodes
===========

prx01
prx02

opencontrail control,config,database cluster
============================================

ntw01
ntw02
ntw03

opencontrail analytic,database cluster
======================================

nal01
nal02
nal03

logging cluster
===============

log01
log02
log03

monitoring cluster
==================

mon01
mon02

metering cluster
================

mtr01
mtr02

compute node
============

cmp001


.. salt model
=========================

.. To create new cluster use:

.. source-code:: bash

  ./reclass-clone.sh cluster.prod.region01 classes/system/openstack classes/system/linux/system classes/system/horizon/server classes/system/salt/control

  # Example:
  ./reclass-clone.sh cluster.region01 classes/system/openstack classes/system/linux/system classes/system/horizon/server classes/system/salt/control


.. To reclass existing tree use:

.. source-code:: bash

  ./reclass -h

  # move nodes under new cluster class
  ./reclass.sh system.linux.system cluster.region01 classes/system/reclass/storage/system/region01*

  # rename class definitions
  ./reclass.sh system.openstack system.openstack-mitaka classes/system/openstack/

  # update cluster classes with cluster preffix
  ./reclass.sh system.openstack cluster.region01 classes/cluster/region01/

  # update cluster classes with cluster preffix & rename class paths
  ./reclass.sh -r 's/openstack./openstack-mitaka./' system.openstack cluster.region03 classes/cluster/region03/system/openstack-mitaka/
